import sys, getopt, enum

SHORT_OPTS="2:3:4:5:6:7:t:p:h"
FULL_OPTS="2nd:3rd:4th:5th:6th:7th:triad:print:help"
USAGE="music_notes.py OPTION VALUE\n<options>\n\t-2 <2nd>\n\t-3 <3rd>\n\t-4 <4th>\n\t" + \
        "-5 <5th>\n\t-6 <6th>\n\t-7 <7th>\n\t-t <root>\ttriad\n\t-p\t\tprint\n\t-h\t\thelp"

class Notes(enum.Enum):
    Unknown=-1
    A=0
    As=1
    B=2
    C=3
    Cs=4
    D=5
    Ds=6
    E=7
    F=8
    Fs=9
    G=10
    Gs=11

OCTAVE=['A','A#','B','C','C#','D','D#','E','F','F#','G','G#']
MINOR=['A','Bb','B','C','Db','D','Eb','E','F','Gb','G','Ab']

def lowerBounds():
        return Notes.A.value

def upperBounds():
        return Notes.Gs.value

def convertNoteToIndex(note):
    if note in OCTAVE:
        return OCTAVE.index(note)
    elif note in MINOR:
        return MINOR.index(note)
    return Unknown

class Chords(enum.Enum):
    UnknownChord=-1
    MAJ=0
    MIN=1
    AUG=2
    DIM=3

class Scales():
    def __init__(self, note):
        self.currNote = note
        self.scale = []
        self.setMajorScale()

    def getCurrNote(self):
        return self.currNote

    def isMajorNote(self):
        return self.currNote == Notes.A.value or \
            self.currNote == Notes.B.value or \
            self.currNote == Notes.C.value or \
            self.currNote == Notes.D.value or \
            self.currNote == Notes.E.value or \
            self.currNote == Notes.F.value or \
            self.currNote == Notes.G.value

    def convertToMinor(self):
        if self.currNote == octave[Notes.As]:
            return minor[Notes.Bb]
        elif self.currNote == octave[Notes.Cs]:
            return minor[Notes.Db]
        elif self.currNote == octave[Notes.Ds]:
            return minor[Notes.Eb]
        elif self.currNote == octave[Notes.Fs]:
            return minor[Note.Gb]
        elif self.currNote == octave[Notes.Gs]:
            return minor[Notes.Ab]

    def setMajorScale(self):
        self.scale.append(self.getCurrNote())       # root note
        self.wholeStepUp()
        self.scale.append(self.getCurrNote())       # tone
        self.wholeStepUp()
        self.scale.append(self.getCurrNote())       # tone
        self.halfStepUp()
        self.scale.append(self.getCurrNote())       # semitone
        self.wholeStepUp()
        self.scale.append(self.getCurrNote())       # tone
        self.wholeStepUp()
        self.scale.append(self.getCurrNote())       # tone
        self.wholeStepUp()
        self.scale.append(self.getCurrNote())       # tone
        self.halfStepUp()
        self.scale.append(self.getCurrNote())       # semitone

    def setNextScale(self):
        while True:
            self.halfStepUp()
            for s in self.scale:
                if self.currNote == s:
                    return

    def halfStepUp(self):
        self.currNote += 1
        if self.currNote > upperBounds():
            self.currNote = Notes.A.value

    def halfStepDown(self):
        self.currNote -= 1
        if self.currNote < lowerBounds():
            self.currNote = Notes.Gs.value

    def wholeStepUp(self):
        self.halfStepUp()
        self.halfStepUp()

    def wholeStepDown(self):
        self.halfStepDown()
        self.halfStepDown()

    def setSecond(self):
        self.wholeStepUp()

    def setThird(self):
        self.wholeStepUp()
        self.wholeStepUp()
    
    def setFourth(self):
        self.setThird()
        self.wholeStepUp()
    
    def setFifth(self):
        self.setFourth()
        self.wholeStepUp()

    def setSixth(self):
        self.setFifth()
        self.wholeStepUp()

    def setSeventh(self):
        self.setSixth()
        self.wholeStepUp()

    def setEigth(self):
        self.setSeventh()
        self.wholeStepUp()

    def setCurrentNote(self, note):
        self.currNote = note

    def setIndexOfScale(idx):
        return self.scale[i]

    def printScale(self):
        print 'Scale:\t',
        self.printNotes(self.scale)

    def printNotes(self, notes):
        for n in notes:
            print '%s' % OCTAVE[n],
        print ''

    def printSheet(self, notes):
        root = Notes.A.value
        curr = Scales(root)
        octave = 0
        idx = 2
        while octave < 2:
            note = curr.getCurrNote()
            if curr.isMajorNote():
                line = ''
                if (octave == 0) and \
                        (note == Notes.D.value or \
                        note == Notes.F.value):
                    line = '--'
                elif octave == 0:
                    line = '  '
                elif (octave == 1) and \
                        (note == Notes.E.value or \
                        note == Notes.G.value or \
                        note == Notes.B.value):
                    line = '--'
                else:
                    line = '  '
                print line,''
                print '{} is {}'.format(note, notes[idx])
                if note == notes[idx]:
                    print 'x',''
                    idx -= 1
                else:
                    print line[0],''
                print line
            if note == Notes.C.value:
                octave += 1
            curr.halfStepDown()

class Triad(Scales):
    def __init__(self, root):
        if isinstance(root, str):
            self.root_idx = convertNoteToIndex(root)
            self.root = Scales(self.root_idx)
        else:
            self.root = Scales(root)
        self.root.printScale()

    def getTriad(self, type):
        if type == Chords.MAJ:
            self.getMajorTriad()
        elif type == Chords.MIN:
            self.getMinorTriad()
        elif type == Chords.AUG:
            self.getAugmentedTriad()
        elif type == Chords.DIM:
            self.getDiminishedTriad()

    def getMajorTriad(self):
        notes = []
        notes.append(self.root.getCurrNote())
        self.root.setNextScale()
        self.root.setNextScale()
        notes.append(self.root.getCurrNote())
        self.root.setNextScale()
        self.root.setNextScale()
        notes.append(self.root.getCurrNote())
        print 'Maj:\t',
        self.root.printNotes(notes)
        self.root.printSheet(notes)
        self.root.setCurrentNote(self.root_idx)

    def getMinorTriad(self):
        notes = []
        notes.append(self.root.getCurrNote())
        self.root.setNextScale()
        self.root.setNextScale()
        self.root.halfStepDown()
        notes.append(self.root.getCurrNote())
        self.root.halfStepUp()
        self.root.setNextScale()
        self.root.setNextScale()
        notes.append(self.root.getCurrNote())
        print 'Min:\t',
        self.root.printNotes(notes)
        self.root.setCurrentNote(self.root_idx)

    def getAugmentedTriad(self):
        notes = []
        notes.append(self.root.getCurrNote())
        self.root.setNextScale()
        self.root.setNextScale()
        notes.append(self.root.getCurrNote())
        self.root.setNextScale()
        self.root.setNextScale()
        self.root.halfStepUp()
        notes.append(self.root.getCurrNote())
        print 'Aug:\t',
        self.root.printNotes(notes)
        self.root.setCurrentNote(self.root_idx)

    def getDiminishedTriad(self):
        notes = []
        notes.append(self.root.getCurrNote())
        self.root.setNextScale()
        self.root.setNextScale()
        self.root.halfStepDown()
        notes.append(self.root.getCurrNote())
        self.root.halfStepUp()
        self.root.setNextScale()
        self.root.setNextScale()
        self.root.halfStepDown()
        notes.append(self.root.getCurrNote())
        print 'Dim:\t',
        self.root.printNotes(notes)
        self.root.setCurrentNote(self.root_idx)

def writeMusicOptions(argv):
    number_opt=0
    number_val=0
    triad = Triad
    out = False
    try:
        opts, args = getopt.getopt(argv[1:], SHORT_OPTS, FULL_OPTS)
        for opt, arg in opts:
            if opt in("-t","--triad"):
                print 'Writing {} triad'.format(arg)
                triad = Triad(arg)
                triad.getTriad(Chords.MAJ)
                triad.getTriad(Chords.MIN)
                triad.getTriad(Chords.AUG)
                triad.getTriad(Chords.DIM)
            elif opt in("-p","--print"):
                out = True
#        if out == True:
#            triad.printSheet()
    except getopt.GetoptError:
        print 'Error: {}'.format(USAGE)

def main(argv):
    writeMusicOptions(argv)

if __name__ == "__main__":
    main(sys.argv)
